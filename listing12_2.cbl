       IDENTIFICATION DIVISION. 
       PROGRAM-ID. LISTING12-2.
       AUTHOR. NAPHAT.
       
       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01  STUDENT_REC.
           02 STUDENT_ID     PIC 9(8)    VALUE 12345678.
           02 GPA            PIC 9V99    VALUE 3.25.
           02 FORENAME       PIC X(6)    VALUE "Matt".
           02 SURNAME        PIC X(8)    VALUE "Cullen".
           02 GENDER         PIC X       VALUE "M".
           02 PHONE_NUMBER   PIC X(14)   VALUE "3536120228233".

       66  PERSONAL_INFO      RENAMES FORENAME    THRU  PHONE_NUMBER.
       66  COLLEGE_INFO       RENAMES STUDENT_ID  THRU  SURNAME.
       66  STUDENT_NAME       RENAMES FORENAME    THRU  SURNAME.  

       01  CONTACT_INFO.
           02 STUD_NAME.
              03 STUD_FORENAME  PIC X(6).
              03 STUD_SURNAME   PIC X(8).
           02 STUD_GENDER       PIC X.
           02 STUD_PHONE        PIC X(14).

       66  MY_PHONE RENAMES STUD_PHONE.

       PROCEDURE DIVISION.
       BEGIN.
           DISPLAY "Example 1"
           DISPLAY "All information = " STUDENT_REC
           DISPLAY "College info    = " COLLEGE_INFO 
           DISPLAY "Personal Info   = " PERSONAL_INFO 

           DISPLAY "Example 2" 
           DISPLAY "Combined names  = " STUDENT_NAME 

           MOVE  PERSONAL_INFO TO CONTACT_INFO

           DISPLAY "Example 3"
           DISPLAY "Name     is " STUD_NAME 
           DISPLAY "Gender   is " STUD_GENDER 
           DISPLAY "Phone    is " STUD_PHONE 

           DISPLAY "Exmaple 4"
           DISPLAY "MyPhone is " MY_PHONE 
           STOP RUN
       .
